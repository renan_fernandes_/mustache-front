<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mustache-teste' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Q~Q854o{<V(uEv#9g.vR|Vhohf[gnpBnFAR^$4Jrd_]T4,$},NvN)zAuiJ;:Jh~%' );
define( 'SECURE_AUTH_KEY',  '9o72&&a0SSE%4%+VGZE@pU)vYBFk5A@JZ6x7_#p%t}T+Q*e*!Q4RM4bt)3U7jt=!' );
define( 'LOGGED_IN_KEY',    'msu0Ably3~$&o%?#y_B34=,BnJ|CowTnUDa9m2hc%{cT!aLw=~J)QTX2u0ZY$ZRx' );
define( 'NONCE_KEY',        '~z?DsLAFsW=:{k|!X3xPBxBDWFF&<c.{:6{VEKqCYRjD@es2<.T_sAun9</qn1ye' );
define( 'AUTH_SALT',        'g?:=|f N_}3qh7zA<^=26T/2 )tcG@r/dGq~).6F7ei4#[{wKL&@Sksh=`nwi+IQ' );
define( 'SECURE_AUTH_SALT', 'O,4S-Gajw^(a{{;.U[&8&OjmM5<}]|SQHIMFhedE~A{0*kxfwL.KO!9gJpU(`x)P' );
define( 'LOGGED_IN_SALT',   '~dB%9!V%tr0_^XiRSW*wlEh{P8Cc3^X+OGY+,r=Gm^x-L+KZi7}e,*Z5XU}xOBUH' );
define( 'NONCE_SALT',       'm+{4Q*dsk%3#j9X~g]jomDdyj+GWI6naVsC3ws`+`P5E%/|nl<9DjbL:}4_dpruI' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
