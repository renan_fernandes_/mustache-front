<?php

add_filter('wpcf7_autop_or_not', '__return_false');

function menuSite() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Menu Site' ),
      'extra-menu' => __( 'Menu Extra' )
    )
  );
}
add_action( 'init', 'menuSite' );

add_theme_support( 'post-thumbnails' );

?>