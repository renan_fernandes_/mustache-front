<!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">

		<title><?php the_title(); ?></title>	

		<!-- Lato Font -->
		<link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet"> 

		<!-- Bootstrap -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

		<!-- Normalize -->
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css">

		<!-- Slick js -->
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
		<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/slick/slick-theme.css"  >

		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css">

		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>
		
		<header id="site-header" class="header-footer-group" role="banner">
			<div class="menu-wrapper">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-site.png" alt="Mustache Teste" class="logo-site btn-site">

				<nav class="menu-site">
					<?php 

						$menuSite = wp_get_nav_menu_items('Menu site');

						foreach ($menuSite as $menuLink) {
							echo 
								"<a href='". $menuLink->url ."' class='menu-site-link'>". $menuLink->post_title ."</a>";
						}

					?>
					
				</nav>		

				<div class="search-box">
					<input type="text" placeholder="Buscar">
				</div>
			</div>

		</header>


