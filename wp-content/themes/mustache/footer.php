
			<footer id="site-footer" role="contentinfo" class="header-footer-group">
				<div class="container">
					<div class="footer-info row justify-content-center">
						<div class="col-md-4 col-sm-10 col-12">
							<div class="footer-info-item">
								<h3 class="info-item-title">Lorem ipsum dolor sit amet</h3>
								<div class="info-item-desc">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bra-icon.jpg" alt="" class="info-item-icon" />
									<p>Lorem ipsum dolor sit amet, 00 <br>
									Lorem ipsum dolor sit amet</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-10 col-12">
							<div class="footer-info-item">
								<h3 class="info-item-title">Lorem ipsum dolor sit amet</h3>
								<div class="info-item-desc">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bra-icon.jpg" alt="" class="info-item-icon" />
									<p>Lorem ipsum dolor sit amet, 00 <br>
									Lorem ipsum dolor sit amet</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-10 col-12">
							<div class="footer-info-item">
								<h3 class="info-item-title">Lorem ipsum dolor sit amet</h3>
								<div class="info-item-desc">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/eua-icon.jpg" alt="" class="info-item-icon" />
									<p>Lorem ipsum dolor sit amet, 00 <br>
									Lorem ipsum dolor sit amet</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-extra-info">
					<span class="copyright">Lorem ipsum dolor sit amet</span>
					<div class="footer-links">
						<a href="#" target="_blank">Lorem Ipsun</a> |
						<a href="#" target="_blank">Lorem Ipsun</a>
					</div>
				</div>
				
			</footer>

		<?php wp_footer(); ?>

		<script
			  src="https://code.jquery.com/jquery-3.4.1.min.js"
			  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
			  crossorigin="anonymous"></script>

	  	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

	  	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

		<script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>



	</body>
</html>
