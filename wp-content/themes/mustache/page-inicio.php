<?php get_header(); ?>

<main id="site-content" role="main">
	<?php $page = get_post(); ?>

	<section id="banner-site" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($page->ID)); ?>')">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/slogan.png" alt="Mustache slogan" class="banner-slogan">
	</section>

	<section id="posts" class="py-3">
		<div class="container">
			<h2 class="title-h2">Conteúdo Interno</h2>
			<div class="post-slider">
				<?php 
					$todosPosts = get_posts(); 			

					foreach ($todosPosts as $postItem) { 
						$postCat = wp_get_object_terms( $postItem->ID, 'category' );
						echo 
						"<div class='post-slider-wrapper'>
							<div class='post-item'>
								<span class='post-item-cat'> ". $postCat[0]->name ."</span>
								<img src='". get_the_post_thumbnail_url( $postItem->ID ) ."' class='post-item-img'>
								<div class='post-item-desc'>
									<h2 class='post-item-title'>" . $postItem->post_title . "</h2>	
									<p class='post-item-text'>". wp_trim_words( $postItem->post_content, 20, '...' ) ."</p>
									<a href='#' class='post-item-link'>Ir para o post</a> 
								</div>
							</div>
						</div>";
					}
				?>						
			</div>
		</div>
	</section>

	<section id="contato" class="py-3">
		<div class="container">
			<h2 class="title-h2">Lorem ipsum dolor sit amet</h2>		
			<?php echo do_shortcode('[contact-form-7 id="20" title="Contato Home" html_class="contato-form"]'); ?>
		</div>
	</section>
	

</main>


<?php get_footer();
