$(document).ready(function(){

	$('.btn-site').on('click', function(){
		$('.menu-site').toggleClass('ativo');
	});


	$('.post-slider').slick({
		autoplay : true,
		dots: true, 
		arrows: true,
		slidesToShow: 3,
 		slidesToScroll: 1,
 		responsive: [
		    {
		      breakpoint: 991,
		      settings: {
		        slidesToShow: 2,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1
		      }
		    }
		  ]
	});

	
});

